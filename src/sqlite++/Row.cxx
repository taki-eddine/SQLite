////////////////////////////////////////////////////////////////////////////////
#include "sqlite++/Row.hxx"
/******************************************************************************/
#include <stdlib.h>
using std::string;
////////////////////////////////////////////////////////////////////////////////
namespace sqlite {

// Methods -----------------------------
Row &
Row::add (const string &column_name, const string &value)
{  _data[column_name] = value;
   return *this;
}

// - accessors - //
/*int
Row::getInt (u32 index)
{  return atoi(_data.at(index).c_str());  }
*/
int
Row::getInt (const string &column_name)
{  return atoi(_data.at(column_name).c_str());  }

const char*
Row::getString (const string &column_name)
{  return _data.at(column_name).c_str();  }

} // namespace sqlite
