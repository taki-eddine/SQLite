////////////////////////////////////////////////////////////////////////////////
#include "sqlite++/Connection.hxx"
/******************************************************************************/
#include <string>
using std::string;
#include "sqlite++/Result.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace sqlite {

// ==== [Private] ==============================================================
// Methods -----------------------------
void
Connection::_checkForErrors ()
{  if ( _last_error_code != OK )
   {  _last_error_msg = _sqlite3_error_msg;
      ::sqlite3_free(_sqlite3_error_msg);
   }
}

// ==== [Public] ===============================================================
// Constructors & Destructors ----------
/**
 * @brief Construct with file name.
 * @param file_name File name path.
**/
Connection::Connection (const char *file_name)
{  open(file_name);  }

/** Close The connection. */
Connection::~Connection ()
{  close();  }

// Methods -----------------------------
/**
 * @brief Open a database connection.
 * @param file_name Database path.
 * @return Result code.
**/
int
Connection::open (const char* file_name)
{  if ( (_last_error_code = ::sqlite3_open(file_name, &_data_base)) != OK )
      _last_error_msg = "Enable to open the database";
   return _last_error_code;
}

/** @brief Close the database connection. **/
void
Connection::close ()
{  ::sqlite3_close(_data_base);  }

// - Query Handling - //
/**
 * @brief Executes a resutl-less query.
 * @param query The SQL query to execute.
 * @return Return true on success, false on failer.
**/
bool
Connection::exec (const char *query)
{  _last_error_code = ::sqlite3_exec(_data_base, query, nullptr, nullptr,
                                     &_sqlite3_error_msg);
   _checkForErrors();
   return !_last_error_code;
}

Result
Connection::query (const char *query)
{  Result result = Result();

   _last_error_code = ::sqlite3_exec(_data_base, query,
                                     [] (void *_result, int ncolumns, char **columns_values, char **columns_names)
                                      -> int
                                     {  Result &result = *static_cast<Result *>(_result);
                                        Row row = Row();

                                        for (int i = 0; i < ncolumns; ++i)
                                           row.add(string(columns_names[i]), string(columns_values[i]));
                                        result.add(row);

                                        return 0;
                                     },
                                     &result, &_sqlite3_error_msg);
   _checkForErrors();
   return result;
}

} // namespace sqlite
