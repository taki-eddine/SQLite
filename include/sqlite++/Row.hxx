////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <string>
#include <map>
#include <cxx/Property.hxx>
////////////////////////////////////////////////////////////////////////////////
namespace sqlite {

class Row
{     using ClassName = Row;
   private:
      // Members ----------------------------
      std::map<std::string, std::string> _data;

   public:
      // Constructors & Destructors ----------
      Row () = default;

      // Methods -----------------------------
      Row & add (const std::string &column_name, const std::string &value);

      // - accessors - //
      int           getInt    (cxx::u32 index);
      int           getInt    (const std::string &column_name);

      unsigned      getUInt   (cxx::u32 i);
      long          getLong   (cxx::u32 i);
      long long     getLLog   (cxx::u32 i);
      float         getFloat  (cxx::u32 i);
      double        getDouble (cxx::u32 i);

      const char *  getString (cxx::u32 i);
      const char *  getString (const std::string &column_name);

      // Properties --------------------------
      _closure_(cxx::u32, Size);
};

// === [Public] ================================================================
// Methods -----------------------------
_force_inline_ cxx::u32
Row::getSize ()
{  return _data.size();  }

} // namespace
