////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <vector>
#include "sqlite++/Row.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace sqlite {

/**
 * @brief The result set of a query statement.
**/
class Result
{     using ClassName = Result;
   private:
      // Methods -----------------------------
      std::vector<Row> _rows;

   public:
      // Constructors & Destructors ----------
      Result () = default;

      // Methods -----------------------------
      void  add (Row new_row);
      Row & at  (cxx::u32 i);

      auto begin ();
      auto end  ();

      // Operators ---------------------------
      const Row & operator [] (cxx::u32 i);
      Result &    operator += (Row &&row);

      // Properties --------------------------
      _closure_(cxx::u32, Size);
};

// ==== [Public] ===============================================================
// Nethods -----------------------------
_force_inline_ auto
Result::begin ()
{  return _rows.begin();  }

_force_inline_ auto
Result::end ()
{  return _rows.end();  }

// Properties --------------------------
_force_inline_ cxx::u32
Result::getSize ()
{  return _rows.size();  }

} // namespace
