////////////////////////////////////////////////////////////////////////////////
#pragma once
#include <cxx/String.hxx>
#include "sqlite++/intern/sqlite3.h"
#include "sqlite++/ErrorCode.hxx"
#include "sqlite++/Result.hxx"
////////////////////////////////////////////////////////////////////////////////
namespace sqlite {

/**
 * @brief Database connection.
**/
class Connection
{     using ClassName = Connection;
      friend class TestConnection;
   private:
      // Data Members ------------------------
      ::sqlite3     *_data_base         = nullptr;
      char          *_sqlite3_error_msg = nullptr;
      int           _last_error_code    = OK;
      cxx::String   _last_error_msg     = cxx::String();

      // Methods -----------------------------
      void _checkForErrors ();

   public:
      // Constructors & Destructors ----------
      Connection  () = default;
      Connection  (const char *file_name);
      ~Connection ();

      // Methods -----------------------------
      int  open  (const char *file_name);
      void close ();

      // - Query Handling - //
      bool   exec  (const char *query);
      Result query (const char *query);

      // Properties --------------------------
      _property_(LastErrorCode, _last_error_code, read_only, get);
      _property_(LastErrorMsg,  _last_error_msg,  read_only, get);
};

} // namespace sqlite
