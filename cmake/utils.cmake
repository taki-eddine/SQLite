# @brief Gives the absolute path of a header file
function(absolute_header_path header_file return)
   execute_process(COMMAND           find /usr/include/ /usr/local/include/ -path "*/${header_file}" -print0
                   WORKING_DIRECTORY .
                   RESULT_VARIABLE   result
                   OUTPUT_VARIABLE   output)
   set(${return} ${output} PARENT_SCOPE)
endfunction()

# @brief Gives the absolute path of a header file only inside the include directory of the project
function(absolute_header_path_proj header_file return)
   execute_process(COMMAND           find ${CMAKE_SOURCE_DIR}/include/ -path "*/${header_file}" -print0
                   WORKING_DIRECTORY .
                   RESULT_VARIABLE   result
                   OUTPUT_VARIABLE   output)
   set(${return} ${output} PARENT_SCOPE)
endfunction()
