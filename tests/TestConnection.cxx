////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
using testing::Test;
#include <cxx/IO.hxx>
#include "sqlite++/Connection.hxx"
using cxx::u32;
////////////////////////////////////////////////////////////////////////////////
namespace sqlite {

class TestConnection : public Test
{  protected:
      virtual void SetUp    () { }
      virtual void TearDown () { }

   public:
      TestConnection  () { }
      ~TestConnection () { }

      void
      open ()
      { }

      void
      close ()
      { }

      void
      exec ()
      { }

      void
      query ()
      {  Result result = Result();
         Connection db = Connection("test_database.db");

         EXPECT_TRUE( db.exec("CREATE TABLE person (id INT PRIMARY KEY, name VARCHAR(30))") );

         EXPECT_TRUE( db.exec("INSERT INTO person VALUES (1, 'P-1');"
                              "INSERT INTO person VALUES (2, 'P-2');"
                              "INSERT INTO person VALUES (3, 'P-3');")
                    );

         result = db.query("SELECT * FROM person");

         EXPECT_EQ(3u, (u32)result.Size);

         for ( Row &row : result )
            cxx::printf("ID = %d, NAME = '%s' \n", row.getInt("id"), row.getString("id"));
      }
};

// Register Test Units -----------------
TEST_F(TestConnection, open)  { open();  }
TEST_F(TestConnection, close) { close(); }
TEST_F(TestConnection, exec)  { exec();  }
TEST_F(TestConnection, query) { query(); }

} // ~ namespace
